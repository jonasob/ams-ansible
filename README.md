# Overview
This is a set of playbooks for managing AMS deployment in the FSFE
infrastructure. The AMS consists of five different subsystems: the
backend supporter database (supporter-db), its associated API 
(fsfe-cd), the authentication provider (oidcp), the user-facing
frontend (frontend) and a reverse proxy (rproxy).

How these fit together is a source of great mystery and confusion,
but here's a rough story which takes you through all of these:

 1. Lisa wants to login to the FSFE system to change her address.
 2. She connects to fsfe.org which, when she clicks to login, directs
    her to id.fsfe.org.
 3. The host id.fsfe.org is the reverse proxy (rproxy): it sits in front
    of many of the other services and proxies connections to them. This
    means the main SSL certificates are usually available only on rproxy
    which then communicates via a private network to the other services.
 4. In the background, rproxy contacts the oidcp and proxies the connection.
 5. The oidcp sends Lisa a login screen where she enters her username and
    password, or asks to have an email link sent to her so she can login
    with it.
 6. Upon successful authentication, the oidcp redirects to the frontend 
    with a signed authentication ticket.
 7. The frontend (actually, we're again talking to the rproxy, since all
    connections to the frontend are proxied through it) validates the
    ticket and presents Lisa with the options, including the one to change
    address.
 8. When displaying information to Lisa, including her old address, frontend
    sends a request to fsfe-cd to read information. To make sure fsfe-cd
    knows that it's Lisa requesting the information, frontend passes the
    signed authentication ticket to fsfe-cd.
 9. After validating the ticket, fsfe-cd gets the necessary information from
    supporter-db and passes it back to frontend.

There are two reasons rproxy sits in front of so many of the connections:

 1. It allows us to simplify SSL handling to one point (though in practice
    rproxy would be two identical servers for redundancy)
 2. It allows us to easily take down and setup necessary AMS services in
    the background: deploying new versions becomes a matter of building a
    new service machine, then simply telling rproxy to start using the new
    machine instead of the previous one.

# What's here?
In this repository you will find the initial playbooks needed to setup and
configure a single KVM via the Proxmox API. It relies on proxmox-kvm and
proxmoxer.

This is all heavily hardcoded for a specific test installation of Proxmox.
Especially with having an hardcoded IP address for the node!
It doesn't scale or work for anyone else, but it should give some ideas :-)

The way the playbook works is as follows:

 1. Ansible sets a number of variables to get the password for the Proxmox
    API and some information about the node to install.
 2. A random port number is generated (used for the console during
    installation)
 3. A new VM is created in Proxmox. This all happens against localhost
    since proxmox-kvm manages the API connection to Proxmox. In our
    FSFE setup, I suppose this should happen on another host which
    has access to the Proxmox API.
 4. The 'args' argument is a key here, it gets information from
    files/args-debian.yml which contain the arguments needed to start
    an unattended installation of Debian, most critically the VMs IP number
    and a direction to the debian-installer preseed script.
 5. The playbook is paused for seven seconds to let the VM be created.
 6. We retrieve a kernel and initrd for the latest stable Debian and place
    this in /tmp/ on the proxmox host.
 7. We start the VM. This runs the retrieved kernel using the supplied initrd
    and directs the console to the TCP port randomly selected previously.
    [NB: This is insecure!]
 8. When the VM spins up, it launches debian-installer, sets its own IP
    number and then uses this to retrieve the preseed script. The script
    give instructions for what the installer should answer to the questions
    posed by debian-installer during the installation.
 9. The preseed script would be great if it could be read from a repository
    directly, but currently this doesn't seem possible. d-i doesn't support
    https connections, so the seed script need to be in a non-https location.
    The script itself is in files/node-debian.seed
 10. When installation is complete, the VM stops.
 11. Ansible waits until the serial output is terminated.
 12. The 'args' in the VM, which forces it to launch with the supplied
     kernel and debian-installer initrd is removed [NB: This doesn't currently
     work]
 13. The VM is again restarted.

# Left to do
 * Ansible should know about the (private) IP ranges from which to allocate
   IP numbers to new nodes.
 * The Playbook should be adaptable so it works for multiple kinds of
   installations (rproxy, fsfe-cd etc).
 * When a service is deployed, we need to have test cases to test it
   directly (without going through rproxy)
 * If tests pass, we then need to update rproxy with new configuration
 * Same test cases should be run against through rproxy

# Bugs
 * debian-installer doesn't support https (it seems?). So it's impossible
   to load the seed file directly from gitlab/git.fsfe.org :-(


